# Core Repo #

Core Repo is a wrapper around Core Data that hides the boiler plate Core Data setup code, automatically handles multi-threaded context usage with nested contexts, and adds some more reasonable methods of querying and creating objects.


## Prerequisites ##
- Compile + link against either the iOS static library or the Mac framework.  Alternatively you can copy the files from the Source folder into your project (excluding either the Repository+iOS or Repository+Mac category depending on your platform)
- Create a data model in Xcode.
- Generate the NSManagedObject subclasses for your entities.


## How it works ##
Core Repo uses nested contexts in the following configuration:

1. Writer context - Singleton on a background thread. This is the root context that writes to disk.
2. UI context - Singleton on the main thread, child of writer context.
3. Background contexts - These are short lived workers that are children of the UI context.

Changes bubble up automatically, you only need to call ```[repository save]``` once.


## Usage ##

#### Configuration ####

##### Typical #####
```objc
[CoreRepo configure:[[[[CoreRepoConfiguration configuration]
    withModelName:@"MyDataModel" andExtension:@"todos"]
    withStoreType:CoreRepoStoreTypeSQLite]
    debug:YES]];
```

##### Use a Repository sub-class #####
```objc
[CoreRepo configure:[[[[[CoreRepoConfiguration configuration]
    withModelName:@"MyDataModel" andExtension:@"todos"]
    withStoreType:CoreRepoStoreTypeSQLite]
    repositoryClass:MyRepository.class]
    debug:YES]];
```

##### With predicate amending hook #####
```objc
[CoreRepo configure:[[[[[CoreRepoConfiguration configuration]
    withModelName:@"MyDataModel" andExtension:@"todos"]
    withStoreType:CoreRepoStoreTypeSQLite]
    debug:YES]
    predicateAmendingBlock:^NSPredicate *(NSPredicate *predicate, Class entityType) {
        // Allows you to alter any predicate used in any fetch request executed thru a Repository instance
        NSPredicate *excludeInactives = [NSPredicate predicateWithFormat:@"active == YES"];
        return [NSCompoundPredicate andPredicateWithSubpredicates:@[predicate, excludeInactives]];
    }];
```


#### [CoreRepo usingRepository:] ####

This method takes care of determining which context to use based on the thread you're on.  If you are on the main thread, you'll be using the repository connected to the UI context.  If you're on a background thread, a repository that references a new background context will be used.


#### Inserting a new record ####
```objc
[CoreRepo usingRepository:^(Repository *repository) {
    Todo *todo = [repository createEntity:Todo.class];
    todo.summary = @"Win the lottery!";
    [repository save];
}];
```

#### Updating records ####
```objc
[CoreRepo usingRepository:^(Repository *repository) {
    for (Todo *todo in [repository allOf:Todo.class]) {
        todo.done = YES;
    }
    [repository save];
}];
```

#### Loading all of a particular entity ####

```objc
[CoreRepo usingRepository:^(Repository *repository) {
  	NSSet *allTodos = [repository allOf:Todo.class];
    NSLog(@"I have %d todos!", [allTodos count]);
}];
```

#### Mac - Create an NSArrayController ####

```objc
__block NSArrayController *arrayController;
[CoreRepo usingRepository:^(Repository *repository) {
    arrayController = [repository arrayControllerForAllOf:Todo.class];
}];
```

See Repository+Mac.h for additional functions related to creating NSArrayControllers


#### iOS - Create an NSFetchedResultsController ####

```objc
__block NSFetchedResultsController *resultsController;
[CoreRepo usingRepository:^(Repository *repository) {
    resultsController = [repository resultsControllerForAllOf:Todo.class];
}];
```

See Repository+iOS.h for additional functions related to creating NSFetchedResultsControllers


## Coming Soon... ##
- More configuration options
- Additional functions in Repository+iOS for controlling the sectionNameKeyPath & cacheName arguments for NSFetchedResultsController
- Actual UIs for the iOS and Mac example apps
