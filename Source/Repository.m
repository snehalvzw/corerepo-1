/*
Copyright © 2012-2013 George Cox

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge,
publish, distribute, sublicense, and/or sell copies of the Software,
and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#import <CoreData/CoreData.h>
#import "Repository.h"
#import "CoreRepoStack.h"
#import "CoreRepo.h"

@implementation Repository {
	CoreRepoStack *_coreRepoStack;
}

-(id)initWithContext:(NSManagedObjectContext *)context {
	if ((self = [super init])) {
		_context = context;
		_coreRepoStack = [CoreRepo stack];
	}
	return self;
}

-(id)get:(id)objectId {
    if (objectId == nil) {
        if (_coreRepoStack.configuration.debug)
	        [_coreRepoStack logDebugInfo:@"Cannot fetch an object with a nil objectId"];
        return nil;
    }

	if ([objectId isKindOfClass:[NSManagedObjectID class]])
		return [self.context objectWithID:objectId];
	else
		return [self get:[_coreRepoStack managedObjectIDFromStringRepresentation:objectId]];
}

-(void)save {
	[self saveContext:self.context];
}

-(void)saveContext:(NSManagedObjectContext *)context {
	void(^_logError)(NSError *) = ^(NSError *error) {
		[_coreRepoStack logDebugInfo:@"\nFailed to save to data store: \n%@", [error localizedDescription]];
		NSArray* detailedErrors = [[error userInfo] objectForKey:NSDetailedErrorsKey];
		if(detailedErrors != nil && [detailedErrors count] > 0)
			for(NSError* detailedError in detailedErrors)
				[_coreRepoStack logDebugInfo:@"DetailedError: %@", [detailedError userInfo]];
		else
			[_coreRepoStack logDebugInfo:@"%@", [error userInfo]];
	};

	if (context == [_coreRepoStack uiContext])
		[_coreRepoStack logDebugInfo:@"Saving UI Context"];
	else if (context == [_coreRepoStack writerContext])
		[_coreRepoStack logDebugInfo:@"Saving Writer Context"];
	else
		[_coreRepoStack logDebugInfo:@"Saving Worker Context"];

	if (context.parentContext == nil) {
		[context performBlock:^{
			NSError *error;
			[context save:&error];
			if (error != nil)
				_logError(error);
		}];
	} else {
        [context performBlockAndWait:^{
            NSError *error;
            [context save:&error];
            if (error != nil)
                _logError(error);
            else
                [self saveContext:context.parentContext];
        }];
	}
}

-(void)deleteObject:(NSManagedObject *)object {
	[self.context deleteObject:object];
}

-(void)deleteObjectThenSave:(NSManagedObject *)object {
	[self deleteObject:object];
	[self save];
}

-(NSManagedObjectID *)objectIDFromString:(NSString *)objectIDString {
	return [_coreRepoStack managedObjectIDFromStringRepresentation:objectIDString];
}

-(id)createEntity:(Class)class {
	return [[class alloc] initWithEntity:[NSEntityDescription entityForName:NSStringFromClass(class) inManagedObjectContext:self.context]
	      insertIntoManagedObjectContext:self.context];
}

-(id)createDetachedEntity:(Class)class {
	return [[class alloc] initWithEntity:[NSEntityDescription entityForName:NSStringFromClass(class) inManagedObjectContext:self.context]
	      insertIntoManagedObjectContext:nil];
}


-(NSFetchRequest *)fetchRequestFor:(Class)class {
	NSEntityDescription *entityDescription = [NSEntityDescription entityForName:NSStringFromClass(class) inManagedObjectContext:self.context];
	NSFetchRequest *request = [[NSFetchRequest alloc] init];
	[request setEntity:entityDescription];
	return request;
}

-(NSArray *)allOf:(Class)class {
	return [self allOf:class withPredicate:nil];
}

-(NSArray *)allOf:(Class)class withPredicate:(NSPredicate *)predicate {
	return [self allOf:class withPredicate:predicate orderedBy:nil];
}

-(NSArray *)allOf:(Class)class withPredicate:(NSPredicate *)predicate orderedBy:(NSArray *)sortDescriptors {
	NSFetchRequest *fetchRequest = [self fetchRequestFor:class];
	[fetchRequest setPredicate:[self amendPredicate:predicate forEntityType:class]];
	[fetchRequest setSortDescriptors:sortDescriptors];
	return [self.context executeFetchRequest:fetchRequest error:nil];
}

-(id)singleOrNilOf:(Class)class withPredicate:(NSPredicate *)predicate {
	NSArray *tempResults = [self allOf:class withPredicate:predicate];
	NSAssert(tempResults != nil && tempResults.count <= 1, @"singleOrNilOf:withPredicate: expects one or no results, there were %lu", (unsigned long)tempResults.count);
	return (tempResults == nil || tempResults.count == 0)
		? nil
		: tempResults[0];
}

-(id)firstOrNilOf:(Class)class withPredicate:(NSPredicate *)predicate {
	NSArray *tempResults = [self allOf:class withPredicate:predicate];
	return (tempResults == nil || tempResults.count == 0)
		? nil
		: tempResults[0];
}

-(id)top:(NSUInteger)topN of:(Class)class withPredicate:(NSPredicate *)predicate orderedBy:(NSArray *)sortDescriptors {
	NSFetchRequest *fetchRequest = [self fetchRequestFor:class];
	[fetchRequest setPredicate:[self amendPredicate:predicate forEntityType:class]];
	[fetchRequest setSortDescriptors:sortDescriptors];
	[fetchRequest setFetchLimit:topN];
	return [self.context executeFetchRequest:fetchRequest error:nil];
}

-(NSUInteger)count:(Class)class {
	return [self count:class withPredicate:nil];
}

-(NSUInteger)count:(Class)class withPredicate:(NSPredicate *)predicate {
	NSError *error;
	NSFetchRequest *request = [self fetchRequestFor:class];
	request.predicate = [self amendPredicate:predicate forEntityType:class];
	NSUInteger result = [self.context countForFetchRequest:request error:&error];
	if (error)
		NSLog(@"Error retrieving result count: %@", error);
	return result;
}

-(NSPredicate *)amendPredicate:(NSPredicate *)predicate forEntityType:(Class)type {
	return (_coreRepoStack.configuration.predicateAmendingBlock != nil)
		? _coreRepoStack.configuration.predicateAmendingBlock(predicate, type)
		: predicate;
}

@end