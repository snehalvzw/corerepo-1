/*
Copyright © 2012-2013 George Cox

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge,
publish, distribute, sublicense, and/or sell copies of the Software,
and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#import <CoreData/CoreData.h>
#import "CoreRepoConfiguration.h"
#import "Repository.h"

@implementation CoreRepoConfiguration

+(instancetype)configuration {
	return [[CoreRepoConfiguration alloc] init];
}

-(instancetype)init {
	if ((self = [super init])) {
		_repositoryClass = Repository.class;
	}
	return self;
}

-(instancetype)withStoreLocation:(NSURL *)storeLocation {
	self.storeLocation = storeLocation;
	return self;
}

-(instancetype)withModelName:(NSString *)modelName andExtension:(NSString *)modelExtension {
	self.modelName = modelName;
	self.modelExtension = modelExtension;
	return self;
}

-(instancetype)withStoreType:(CoreRepoStoreType)storeType {
	self.storeType = storeType;
	return self;
}

-(instancetype)debug:(BOOL)debug {
	self.debug = debug;
	return self;
}

-(instancetype)repositoryClass:(Class)repositoryClass {
	self.repositoryClass = repositoryClass;
	return self;
}

-(instancetype)predicateAmendingBlock:(NSPredicate *(^)(NSPredicate *, Class entityType))predicateAmendingBlock {
	self.predicateAmendingBlock = predicateAmendingBlock;
	return self;
}

-(NSString *)coreDataStoreType {
	switch (self.storeType) {
		case CoreRepoStoreTypeSQLite:
	        return NSSQLiteStoreType;
		case CoreRepoStoreTypeMemory:
			return NSInMemoryStoreType;
		default:
			return NSInMemoryStoreType;
	}
}


-(NSURL *)storeLocation {
	if (_storeLocation)
		return _storeLocation;

	NSFileManager *fileManager = [NSFileManager defaultManager];
	NSURL *appSupportURL = [[fileManager URLsForDirectory:NSApplicationSupportDirectory inDomains:NSUserDomainMask] lastObject];
	return [appSupportURL URLByAppendingPathComponent:[[NSBundle mainBundle] bundleIdentifier]];
}

-(NSString *)modelName {
	if (_modelName)
		return _modelName;

	self.modelName = @"model";
	return _modelName;
}

-(NSString *)modelExtension {
	if (_modelExtension)
		return _modelExtension;

	self.modelExtension = @"sqlite";
	return _modelExtension;
}

-(CoreRepoStoreType)storeType {
	return _storeType;
}

@end