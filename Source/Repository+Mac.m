/*
Copyright © 2012-2013 George Cox

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge,
publish, distribute, sublicense, and/or sell copies of the Software,
and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#import "Repository+Mac.h"

@implementation Repository (Mac)

-(NSArrayController *)arrayControllerForAllOf:(Class)class {
	NSArrayController *controller = [self unpreparedArrayControllerForAllOf:class];
	[controller prepareContent];
	return controller;
}

-(NSArrayController *)arrayControllerForAllOf:(Class)class where:(NSPredicate *)predicate {
	NSArrayController *controller = [self unpreparedArrayControllerForAllOf:class where:predicate];
	[controller prepareContent];
	return controller;
}

-(NSArrayController *)arrayControllerForAllOf:(Class)class where:(NSPredicate *)predicate orderedBy:(NSArray *)sortDescriptors {
	NSArrayController *controller = [self unpreparedArrayControllerForAllOf:class where:predicate];
	[controller setSortDescriptors:sortDescriptors];
	[controller prepareContent];
	return controller;
}

-(NSArrayController *)unpreparedArrayControllerForAllOf:(Class)class {
	NSArrayController *controller = [[NSArrayController alloc] init];
	
	[controller setManagedObjectContext:self.context];
	[controller setEntityName:NSStringFromClass(class)];
	[controller setEditable:NO];
	[controller setAutomaticallyPreparesContent:YES];
	[controller setAutomaticallyRearrangesObjects:NO];
	[controller setPreservesSelection:NO];
	return controller;
}

-(NSArrayController *)unpreparedArrayControllerForAllOf:(Class)class where:(NSPredicate *)predicate {
	NSArrayController *controller = [self unpreparedArrayControllerForAllOf:class];
	[controller setFetchPredicate:predicate];
	return controller;
}

-(NSArrayController *)unpreparedArrayControllerForAllOf:(Class)class where:(NSPredicate *)predicate orderedBy:(NSArray *)sortDescriptors {
	NSArrayController *controller = [self unpreparedArrayControllerForAllOf:class where:predicate];
	[controller setSortDescriptors:sortDescriptors];
	return controller;
}

@end
